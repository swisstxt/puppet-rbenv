# Class: rbenv
#
# This module manages puppet-rbenv
#
# Parameters:
# - *user*: the user who is going to install rbenv. defaults to $USER
# - *compile*: whether or not a ruby version will be compiled
# - *version*: the global ruby version which is going to be compiled and
#   installed. It is optional.
#
# Actions:
#
# Requires:
# - git and curl
# - Some packages for compiling native extensions
#
# Sample Usage:
#
#     class { "rbenv":
#         user     => "alup",
#         group    => "users",
#         home_dir => "/project/alup",
#         compile  => true,
#         version  => "1.9.3-p0",
#     }
#
# [Remember: No empty lines between comments and class definition]
class rbenv ( 
		$user,
		$project_dir	=   '',
		$group        =   $user,
		$compile      =   $rbenv::params::compile,
		$version      =   $rbenv::params::version 
	) inherits rbenv::params {

  if $project_dir == '' {   
		$home_dir = $user ? {
			'root' => '/root',
      default => "/home/${user}"
		}
	}
  else { $home_dir = $project_dir }

  if $user == 'root' { $root_dir = '/opt' }
	else { $root_dir = $home_dir  }

	if $user == 'root' { $install_dir  = 'rbenv' }
	else { $install_dir  = '.rbenv' }

  rbenv::user { 
    "rbenv_class_${version}_for_${user}":
      user => $user,
      group => $group,
      home_dir => $home_dir,
      root_dir => $root_dir,
      install_dir => $install_dir,
      compile => $compile,
      version => $version;
  }
}
