define rbenv::install ( $user, $group, $home_dir, $root_dir, $install_dir ) {

  # STEP 1
  exec { "rbenv::install::${user}::checkout":
    command => "git clone -v https://github.com/sstephenson/rbenv.git ${install_dir}",
    user    => $user,
    group   => $group,
    cwd     => $root_dir,
    creates => "${root_dir}/${install_dir}",
    path    => ['/usr/bin', '/usr/sbin'],
    timeout => 100,
    require => Package['git'],
  }

  # STEP 2
  exec { "rbenv::install::${user}::add_path_to_bashrc":
    command => "echo \"export PATH=${root_dir}/${install_dir}/bin:\\\$PATH\" >> .bashrc",
    user    => "$user",
    group   => "$group",
    cwd     => "$home_dir",
    onlyif  => "[ -f ${home_dir}/.bashrc ]",
    unless  => "grep ${install_dir}/bin ${home_dir}/.bashrc 2>/dev/null",
    path    => ['/bin', '/usr/bin', '/usr/sbin'],
  }

  # STEP 3
  exec { "rbenv::install::${user}::add_rbenvroot_to_bashrc":
    command => "echo \"export RBENV_ROOT=${root_dir}/${install_dir}\" >> .bashrc; export RBENV_ROOT=${root_dir}/${install_dir}",
    user    => $user,
    group   => $group,
    cwd     => $home_dir,
    onlyif  => "[ -f ${home_dir}/.bashrc ]",
    unless  => "grep RBENV_ROOT ${home_dir}/.bashrc 2>/dev/null",
    path    => ['/bin', '/usr/bin', '/usr/sbin'],
    require => Exec["rbenv::install::${user}::add_path_to_bashrc"],
  }

  # STEP 4
  exec { "rbenv::install::${user}::add_init_to_bashrc":
    command => 'echo "eval \"\$(rbenv init -)\"" >> .bashrc',
    user    => $user,
    group   => $group,
    cwd     => $home_dir,
    onlyif  => "[ -f ${home_dir}/.bashrc ]",
    unless  => "grep 'rbenv init -' ${home_dir}/.bashrc 2>/dev/null",
    path    => ['/bin', '/usr/bin', '/usr/sbin'],
    require => Exec["rbenv::install::${user}::add_rbenvroot_to_bashrc"],
  }

  file { "rbenv::install::${user}::make_plugins_dir":
    ensure  => directory,
    path    => "${root_dir}/${install_dir}/plugins",
    owner   => $user,
    group   => $group,
    require => Exec["rbenv::install::${user}::checkout"],
  }

  # STEP 5
  # Install ruby-build under rbenv plugins directory
  exec { "rbenv::install::${user}::checkout_ruby_build":
    command => 'git clone https://github.com/sstephenson/ruby-build.git',
    user    => $user,
    group   => $group,
    cwd     => "${root_dir}/${install_dir}/plugins",
    creates => "${root_dir}/${install_dir}/plugins/ruby-build",
    path    => ['/usr/bin', '/usr/sbin'],
    timeout => 100,
    require => File["rbenv::install::${user}::make_plugins_dir"],
  }

  exec { "restart_shell":
    path		=> '/bin:/usr/bin',
    command => "/bin/sh -c exec \$SHELL",
    require => Exec["rbenv::install::${user}::checkout_ruby_build"],
    onlyif  => "test `rbenv install & echo \$?` -ne 0"
  }

  # TODO: Support old way of non-plugin installation for ruby-build
  # STEP 6
  #  exec { 'install ruby-build':
  #    command => 'sh install.sh',
  #  user    => 'root',
  #  group   => 'root',
  #  cwd     => "${root_dir}/ruby-build",
  #  onlyif  => '[ -z "$(which ruby-build)" ]',
  #  path    => ['/bin', '/usr/local/bin', '/usr/bin', '/usr/sbin'],
  #  require => Exec["rbenv::install::${user}::checkout_ruby_build"],
  #}
}
