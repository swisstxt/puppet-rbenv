class rbenv::params {
  $group				=		$user
	$version      =   '1.9.3-p194'
  $compile      =   true
	$home_dir = $user ? {
  	'root'        => '/root',
    default				=> "/home/${user}",
  }
  $root_dir = $user ? {
  	'root'        => '/opt',
    default 			=> $home_dir,
  }
  $install_dir = $user ? {
  	'root'        => 'rbenv',
    default 			=> '.rbenv',
  }
}
